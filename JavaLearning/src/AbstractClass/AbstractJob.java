package AbstractClass;

public abstract class AbstractJob {
    public AbstractJob() {
    }
    public abstract String getJobName();
    public abstract void doJob();
}
