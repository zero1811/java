import java.util.Scanner;

public class ClString {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập tên : ");
        String name = scanner.nextLine();
        int index = name.indexOf(" ");
        int lastIndex = name.lastIndexOf(" ");
        String firstName = name.substring(0,index);
        String middleName = name.substring(index,lastIndex);
        String lastName = name.substring(lastIndex);
        System.out.println("Họ : "+firstName +" Đệm : "+ middleName +" Tên : "+lastName);
        System.out.println();
        System.out.println("Nhập chuỗi cần kiểm tra : ");
        String stri = scanner.nextLine();
        while (stri.length() <= 8){
            System.out.println("Nhập lại : ");
            stri = scanner.nextLine();
        }
        System.out.println("Chuỗi vừa nhập là : " +stri);
        System.out.println();
        System.out.println("Nhập chuỗi : ");
        String str = scanner.nextLine();
        String [] arrStr = str.split("");
        for (int i = arrStr.length; i >=0 ; i--) {
            System.out.print(arrStr[i]);
        }
    }
}
