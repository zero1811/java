import java.util.Scanner;

public class Helloworld {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập a : ");
        int a = scanner.nextInt();
        System.out.println("Nhập b : ");
        int b = scanner.nextInt();
        if (a > b)
            System.out.printf("%d > %d",a,b);
        else if (a == b)
            System.out.printf("%d = %d",a,b);
        else
            System.out.printf("%d < %d",a,b);
        System.out.println();
        System.out.println("Nhập số thực n : ");
        float n = scanner.nextInt();
        if (n < 0)
            n = -n;
        System.out.println("Giá trị tuyệt đối của n là : " + n);
        System.out.println("Nhập số thứ nhất : ");
        float n1 = scanner.nextFloat();
        System.out.println("Nhập số thứ hai : ");
        float n2 = scanner.nextFloat();
        System.out.println("Nhập số thứ ba : ");
        float n3 = scanner.nextFloat();
        System.out.println("Số lớn nhất là : " + finMax(n1,n2,n3));
    }
    public static float finMax(float a,float b,float c){
        float max = 0;
        if (a > b)
            max = a;
        else
            max = b;
        if (max < c)
            max = c;
        return max;
    }


}
