package HomeWork.Day1_Array;

import java.util.Arrays;
import java.util.Scanner;

public class Excercise01 {
    public static void main(String[] args) {
        System.out.println("Nhập số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int arr[] = new int[size];
        System.out.println("Nhập mảng :v ");
        for (int i = 0; i < size; i++) {
            System.out.println("A[" +"["+i+"] : ");
            arr[i] = scanner.nextInt();
        }
        System.out.println("Mảng vừa nhập là : ");
        System.out.println();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " ");
        }
        System.out.println();
        while (true){
            menu();
            int chosse = scanner.nextInt();
            switch (chosse){
                case 1 : {
                    Arrays.sort(arr);
                    printArray(arr);
                    break;
                }
                case 2 : {
                    sortRedue(arr);
                    printArray(arr);
                    break;
                }
                default: return;
            }
        }
    }
    public static void menu(){
        System.out.println("1. Sắp xếp tăng dần.");
        System.out.println("2. Sắp xếp giảm giần.");
        System.out.println("3. Thoát.");
    }
    public static void printArray(int a[]){
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
    public static void sortRedue(int a[]){
        for (int i = 0; i < a.length-1; i++) {
            for (int j = i+1; j < a.length; j++) {
                if (a[i] < a[j]){
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }
    }
}
