package HomeWork.Day1_Array;

import java.util.Scanner;

public class Excercise02 {
    public static void main(String[] args) {
        System.out.println("Nhập số phần tử của mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int arr[] = new int[size];
        int sum = 0;
        System.out.println("Nhập mảng : ");
        for (int i = 0; i < size; i++) {
            System.out.println("A[" + i + "] : ");
            arr[i] = scanner.nextInt();
            sum += arr[i];
        }
        System.out.println("Tổng các phần tử trong mảng là : " + sum);
    }
}
