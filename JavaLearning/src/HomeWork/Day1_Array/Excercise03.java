package HomeWork.Day1_Array;

import java.util.Scanner;

public class Excercise03 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        int [] arr =  new int[size];
        scanArray(arr);
        for (int i = 0; i < arr.length; i++) {
            if (checkPrime(arr[i]))
                System.out.print(arr[i] + " ");
        }
    }
    //Kiểm tra số nguyên tố
    public static boolean checkPrime(int number){
        if (number < 2 )
            return false;
        else {
            for (int i = 2; i < number-1; i++) {
                if (number % i == 0){
                    return false;
                }
            }
            return true;
        }
    }
    //Nhập mảng
    public static void scanArray(int a[]){
        for (int i = 0; i < a.length; i++) {
            System.out.println("A["+i+"] : ");
            a[i] = scanner.nextInt();
        }
    }
}
