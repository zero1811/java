package HomeWork.Day1_Array;

import java.util.Scanner;

public class Excercise05 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        scanArray(arr);
        System.out.println("Nhập số nguyên k : ");
        int k = scanner.nextInt();
        System.out.println("Phần tử tại vị trí "+ k+" trong mảng là : "+arr[k]);
    }
    //Nhập mảng
    public static void scanArray(int a[]){
        for (int i = 0; i < a.length; i++) {
            System.out.println("A["+i+"] : ");
            a[i] = scanner.nextInt();
        }
    }
}
