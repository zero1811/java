package HomeWork.Day1_Array;

import java.util.Scanner;

public class Excercise07 {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        int[] arr = new int[size];
        scanArray(arr);
        getMax(arr);
        getMin(arr);

    }
    //Nhập mảng
    public static void scanArray(int a[]){
        for (int i = 0; i < a.length; i++) {
            System.out.println("A["+i+"] : ");
            a[i] = scanner.nextInt();
        }
    }
    public static void getMax(int a[]){
        int max = a[0];
        int indexMax = 0;
        for (int i = 0; i < a.length; i++) {
            if (max < a[i]){
                max = a[i];
                indexMax = i;
            }
        }
        System.out.println("Max = "+max +" Vị trí : "+indexMax);
    }
    public static void getMin(int a[]){
        int min = a[0];
        int indexMin = 0;
        for (int i = 0; i < a.length; i++) {
            if (min > a[i]){
                min = a[i];
                indexMin = i;
            }
        }
        System.out.println("Min = "+min +" Vị trí : "+indexMin);
    }
}
