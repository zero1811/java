package HomeWork.Day1_String;

import java.util.Scanner;

public class Excercise13 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Nhập chuỗi : ");
        String stringInput = scanner.nextLine();
        System.out.println("Nhập ký tự cần kiểm tra : ");
        String character = scanner.next();
        int count = 0;
        for (int i = 0; i < stringInput.length(); i++) {
            if (character.equalsIgnoreCase(String.valueOf(stringInput.charAt(i)))){
                count++;
            }
        }
        if (count == 0)
            System.out.println("Ký tự "+character+" không xuất hiện trong chuỗi "+stringInput);
        else
            System.out.println("Ký tự "+character+" xuất hiện "+count+" lần trong chuỗi "+stringInput);
    }
}
