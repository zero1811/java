package StudentManager;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<Student>();
        do {
            menu();
            Scanner scanner = new Scanner(System.in);
            int chosse = scanner.nextInt();
            switch (chosse){
                case 1 : {
                    importList(list);
                    break;
                }
                case 2 : {
                    export(list);
                    break;
                }
                case 3 : {
                    findStudent(list);
                    break;
                }
                default:return;
            }
        }while (true);
    }
    public static void menu(){
        System.out.println("Mời bạn chọn : ");
        System.out.println("1. Nhập danh sách");
        System.out.println("2. Hiển thị danh sách");
        System.out.println("3. Tìm sinh viên theo điểm");
    }
    public static void importList(ArrayList<Student> list){
        do {
            Student student = new Student();
            System.out.println("Nhập sinh viên : ");
            student.input();
            list.add(student);
            System.out.println("Bạn có muốn nhập tiếp : (Y/N)");
            Scanner scanner = new Scanner(System.in);
            String choose = scanner.next();
            if (choose.equalsIgnoreCase("N")){
                return;
            }
        }while (true);
    }
    public static void export(ArrayList<Student> list){
        for (int i = 0; i < list.size(); i++) {
            System.out.println("Sinh viên : ");
            System.out.println(list.get(i).toString());
        }
    }
    public static void findStudent(ArrayList<Student> list){
        System.out.println("Nhập điểm cần tìm : ");
        Scanner scanner = new Scanner(System.in);
        double score = scanner.nextDouble();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getScores1() == score){
                System.out.println("Sinh viên " + (i+1) +" : ");
                System.out.println(list.get(i).toString());
            }
        }
    }
}
