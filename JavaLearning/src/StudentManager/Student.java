package StudentManager;

import java.util.Scanner;

public class Student {
    private String studentCode;
    private String fullName;
    private String subject;
    private double scores1;
    private double scores2;

    public Student() {
    }

    public Student(String studentCode, String fullName, String subject, double scores1, double scores2) {
        this.studentCode = studentCode;
        this.fullName = fullName;
        this.subject = subject;
        this.scores1 = scores1;
        this.scores2 = scores2;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public double getScores1() {
        return scores1;
    }

    public void setScores1(double scores1) {
        this.scores1 = scores1;
    }

    public double getScores2() {
        return scores2;
    }

    public void setScores2(double scores2) {
        this.scores2 = scores2;
    }
    void input(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập mã sinh viên : ");
        String studentCode = scanner.nextLine();
        this.setStudentCode(studentCode);
        System.out.println("Nhập họ tên sinh viên : ");
        String fullName = scanner.nextLine();
        this.setFullName(fullName);
        System.out.println("Nhập môn học : ");
        String subject = scanner.nextLine();
        this.setSubject(subject);
        System.out.println("Nhập điểm kì I : ");
        double scores1 = scanner.nextDouble();
        this.setScores1(scores1);
        System.out.println("Nhập điểm kì II : ");
        double scores2 = scanner.nextDouble();
        this.setScores2(scores2);
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentCode='" + this.getStudentCode() + '\'' +
                ", fullName='" + this.getFullName() + '\'' +
                ", subject='" + this.getSubject() + '\'' +
                ", scores1=" + this.getScores1() +
                ", scores2=" + this.getScores2() +
                '}';
    }
}
